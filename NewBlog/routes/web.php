<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//pages...
Route::get('/' , 'pages@FrontPage')->Middleware('guest');
Route::get('signup','pages@signup')->Middleware('guest');
Route::get('login','pages@login')->Middleware('guest');
Route::get('UApp','pages@UserApp')->Middleware('auth');
Route::get('home','pages@home')->Middleware('auth');
Route::get('profile/{id}','profilecontroller@edit');
Route::get('posts','post_controller@index')->Middleware('auth');
Route::get('post/show/{id}','post_controller@show');
Route::get('post/update/{id}','post_controller@edit');
Route::get('/SignupAsComp' , 'CompanyController@index');


Route::get('/userstable','usercontroller@userstable');
Route::get('/UserUpdate/{id}','UserController@edit');


Route::get('job','pages@job');

Route::post('job','jobcontroller@job');

//process
Route::post('signup','UserController@NewUser');
Route::post('login','usercontroller@login');
Route::post('home','post_controller@store');
Route::post('post/update/{id}','post_controller@update');
Route::delete('post/delete/{id}','post_controller@destroy');
Route::post('UApp','UserAppcontroller@insert');
Route::post('/UploadAvatar' , 'AvatarController@store');
Route::post('/SignupAsComp','CompanyController@store');

Route::post('/UserUpdate/{id}','usercontroller@update');
Route::post('/UserDelete/{id}','usercontroller@delete');

//logout
Route::get('/logout','pages@logout');




// Route::get('post_show/{id}',function($id){

// 	$post = App\post::find($id);

// 	return view('pages.post_show')->with('post',$post);
// })->middleware('auth');
Route::get('/test','testcontroller@test');

 Route::get('/test','testcontroller@test');


// Route::get('AdminDashbord',function(){
// 	return 'welcome admin';
// })->middleware('Auth','Role:Admin');