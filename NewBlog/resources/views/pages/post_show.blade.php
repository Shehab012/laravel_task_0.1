@extends('layouts.appu')
@section('content')
	<div>
		<a class="btn btn-info btn-block" href="/posts">Back</a> 
	</div>

	<div style="background-color: snow; text-align: center; border: solid 1px lightgray;" class="well">
		<p><small> {!! $data->title !!} </small></p>
		<hr>
		<h3><b> {{ $data->body }} </b></h3>	
		<div class="pull-right">
			<small> {{ $data->created_at }} </small> 
		</div>
	</div>

	{!! Form::open(['Action'=>'/post/delete/$data->id' , 'method'=>'delete']) !!}
	<div style="width: 50%; float: left;">
		<a href="/post/delete/{{$data->id}}" type="submit" class="btn btn-danger btn-block" >Delete Post</a>
	</div>
	{!! Form::close() !!}

	<div style="width: 50%; float: left;">
		<a href="/post/update/{{$data->id}}" type="submit" class="btn btn-primary btn-block" >Update Post</a>
	</div>

@endsection