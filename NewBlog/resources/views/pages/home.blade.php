@extends('layouts.appu')
@section('content') 
welcome  {{Auth::User()->FirstName}}
    {!! Form::open(['Action'=>'post_controller@store','method'=>'post']) !!}
            <div>
                {{Form::label('title','subject')}}
                {{Form::text('title','',['class'=>'form-control','placeholder'=>'the subject'])}}
            </div>

            <div>
                {{Form::label('','write what are you want')}}
                {{Form::textarea('body','',['class'=>'form-control','placeholder'=>'This a text area'])}}
            </div>
            <div>
                {{Form::submit('submit',['class'=>'btn btn-primary','style'=>'margin-bottom:3px;'])}}
            </div>
    {!! Form::close() !!}

@endsection