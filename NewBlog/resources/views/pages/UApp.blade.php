@extends('layouts.appu')
@section('content')
    <div class="col-md-12">
        <div class="row">

            <div class="col-md-2">
                <div class="row">
                
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <!--
                    User App submit form
                    -->
                    <form method="post" action="UApp" enctype= "multipart/form-data" >
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your Phone</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-earphone"></span></span>
                                <input type="text" name="phone" placeholder="Enter your phone" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>nationality</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag"></span></span>
                                <input type="text" name="nationality" placeholder="Enter your nationality" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/>
                    <br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your city town</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag"></span></span>
                                <input type="text" name="city" placeholder="Enter your city" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    
                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your gender</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag"></span></span>
                                <input type="text" name="gender" placeholder="Enter your gender" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                     <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your college</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-institution"></span></span>
                                <input type="text" name="college" placeholder="Enter your college" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your education</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-mortar-board"></span></span>
                                <input type="text" name="education" placeholder=" your education" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your great</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-percent"></span></span>
                                <input type="text" name="great" placeholder=" your great" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your experience</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-briefcase"></span></span>
                                <textarea rows="3" name="experience" placeholder=" your experience" class="form-control" aria-describedby="basic-addon1"></textarea>
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>birthdate</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar"></span></span>
                                <input type="date" name="birthdate" class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>military status</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-flag"></span></span>
                                <input type="text" name="military_status"  class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Marital Status</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-address-book"></span></span>
                                <input type="text" name="Marital Status"  class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="row">
                                <h4>Your Cv</h4>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="fa fa-upload"></span></span>
                                <input type="file" name="CV"  class="form-control" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        
                        </div>
                    </div>

                    <br/><br/>

                    <div class="col-md-12">
                        <div class="row">
                           <button class="btn btn-primary btn-block">Send</button>
                        </div>
                    </div>
                    {{ csrf_field() }}
                </form>
                    
                    
                </div>
            </div>

            <div class="col-md-2">
                <div class="row">
                
                </div>
            </div>

        </div>
    </div>
@endsection