@extends('layouts.appu')

@section('content')

    <div class="col-md-12" >
        <div class="row" >

            <!--
                Profile heeader
            -->
                        
            <div class="col-md-12 profile-header" >
                <div class="row">
                	<div class="col-md-3">
                         <div class="row">
                          <h3 style="color: blue;"> {{ Auth::user()->FirstName }} </h3>
                          <p> {{ Auth::user()->Status }} </p>   
                         </div>   
                    </div>

                    <div class="col-md-3">
                         <div class="row">
                             
                         </div>   
                    </div>

                    <div class="col-md-3">
                         <div class="row">
                             
                         </div>   
                    </div>

                    <div class="col-md-3">
                         <div class="row">
                             <a href="#"><span class="fa fa-facebook"></span></a>
                             <a href="#"><span class="fa fa-google"></span></a>
                             <a href="#"><span class="fa fa-twitter"></span></a>
                             <a href="#"><span class="fa fa-linkedin"></span></a>
                         </div>   
                    </div>

                </div>
            </div>
            <!--
                Profile body
            -->
            <div class="col-md-12">
                <div class="row">
                    
                    <div class="col-md-0"><div class="row"></div></div>

                    <div class="col-md-12 profile-body">
                        <div class="row">
                    <!--
                    right culom
                    -->
                            <div class="col-md-4 profile-body-right">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <form action="/UploadAvatar" method="POST" 
                                             enctype="multipart/form-data">
                                             <img src="{{asset('imges/noimage.png')}}">
                                             @foreach($image as $AV)
                                                 @if($AV->avatar)
                                                      <img src="{{ asset ('imges/'.$AV->avatar)}}"
                                                 @endif
                                            @endforeach
                                             
                                                

                                                 <input type="file" name="Avatar" 
                                                 class="btn btn-default btn-block">

                                                 <button type="submit" class="btn btn-block btn-primary">
                                                 <span class="fa fa-send"> Sned</span></button>
                                                 {{ csrf_field() }}

                                            </form>
                                           
                                            <h3>{{Auth::user()->FirstName}} {{Auth::user()->LastName}} </h3>
                                            <p>{{Auth::user()->Role}} </p>
                                        </div>
                                    </div>
                                   

                                    <div class="col-md-12">
                                        <div class="row">
                                            <a href="/UserUpdate/{{Auth::id()}}" 
                                            class="btn btn-primary btn-block">Edit my profile</a>
                                            
                                            <form action="/UserDelete/{{Auth::user()->id}}" method="POST">
                                                 <button type="submit" class="btn btn-block btn-danger">
                                                 Delete my acount</button>
                                                 {{ csrf_field() }}
                                            </form>
                                           
                                        </div>
                                    </div>


                                </div>
                            </div>

                    <!--
                    left culom
                     -->

                     <div class="col-md-8 profile-body-left">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                  <h4>About me</h4>
                                  <p>The oldest classical Greek and Latin writing had little or no space between words and could be written in boustrophedon </p>  
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <h4>web service</h4>
                                </div>
                            </div>

                            <div class="col-md-12" style="text-align: center;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <ul>
                                                <li><span class="fa fa-facebook"> </span> <b>facebook</b>: 
                                                <a  href="#">https//facebook.com/eg.ogr </a></li>

                                                <li><span class="fa fa-twitter"> </span> <b>facebook</b>: 
                                                <a  href="#">https//twitter.com/eg.ogr </a></li>

                                                <li><span class="fa fa-google-plus"> </span> <b>facebook</b>: 
                                                <a  href="#">https//google.com/eg.ogr </a></li>

                                                <li><span class="fa fa-linkedin"> </span> <b>facebook</b>: 
                                                <a  href="#">https//linkedin.com/eg.ogr </a></li>

                                                <li><span class="fa fa-android"> </span> <b>facebook</b>: 
                                                <a  href="#">https//are app.com/eg.ogr </a></li>
                                            </ul>  
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">

                                            <ul class="list-group"> 

                                                <li class="list-group-item list-group-item-info">
                                                <b style="font-family: arial;">
                                                 My phone number : {{ $data->phone }} </b>

                                                <li class="list-group-item"><b> City : {{ $data->city }}</b>

                                                <li class="list-group-item"><b> College : {{$data->college}}</b>

                                                <li class="list-group-item">
                                                <b> education : {{ $data->education }} </b></li>

                                                <li class="list-group-item">
                                                <b> Greate : {{ $data->great }} </b></li>

                                                <li class="list-group-item">
                                                <b> experience : {{ $data->  experience }} </b></li>

                                                <li class="list-group-item"><b> Sex : {{$data->gender}}</b>

                                                <li class="list-group-item">
                                                <b> military status : {{$data->military_status }}</b>

                                                <li class="list-group-item">
                                                <b>  Your status : {{$data-> Marital_Status }}</b>
                                                
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    @if(count($posts)>0)
                                        @foreach($posts as $post)
                                             {{$post->body}}  
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>



                        </div>
                    </div>

                    <div class="col-md-0"><div class="row"></div></div>

                </div>
            </div>



            <!--
                Closing tags
            -->
        </div>
    </div>


@endsection