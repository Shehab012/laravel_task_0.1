@extends('layouts.appu')

@section('content')
 <h1> Post show </h1>
    @if(count($posts) >0)
        @foreach($posts as $post)
            <div class="well">
                <p><b><a href="post/show/{{$post->id}}">{{$post->title}}</a></b></p>
                <p><b>{{$post->body}}</b></p>
                <p><small>{{$post->date()}}</small></p>
            </div>
        @endforeach
         {{--  {{$posts->links()}}  --}}
    @else
        <div class="well">
            <p>No posts</p>
        </div>
    @endif
@endsection