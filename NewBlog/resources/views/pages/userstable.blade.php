@extends('layouts.appu')
@section('content')
				<div class="well" style="background-color: snow;">
					<table class="table table-hover">

						<thead>
							<tr>
								<th>User id</th>
								<th>User Name</th>
								<th>User Email</th>
								<th>Role</th>
								<th>Status</th>
								<th>Created at</th>
							</tr>
							
						</thead>

		    @foreach($users as $k => $user)
			    @if($k < 20)
					<tbody>
						<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->FirstName}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->Role}}</td>
						<td>{{$user->Status}}</td>
						<td>{{$user->created_at}}</td>
					</tr>
					</tbody>
				@endif
			@endforeach				
					</table>
				</div>
			
@endsection