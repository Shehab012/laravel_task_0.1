@extends('layouts.app')

@section('content')
    <h1> login </h1>
    <div class="col-md-12">
        <div class="row">
          <div class=col-md-3>
            <div class="row">
            </div>
          </div>

           <div class=col-md-6>
            <div class="row">
                <!--
                form login control
                -->
                <form class="form-group" action="login" method="post">
                <input class="form-control" type="text" name="email" placeholder="enter your email account" required="">
                <input class="form-control" type="password" name="password" placeholder="enter your password" required="">
                {{csrf_field()}}
                <button class="btn btn-primary btn-block" type="submit">Login</button>
                </form>
            </div>
          </div>

           <div class=col-md-3>
            <div class="row">
            </div>
          </div>
        </div>
    </div>


@endsection