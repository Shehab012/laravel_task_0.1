@extends('layouts.app')
@section('content')
  <div class="col-md-12">
	  	<div class="col-md-1"></div>

		  	<div class="col-md-10 company-signup">
		  		<div class="row">
			  			<form action="/SignupAsComp" method="POST" class="form-group">
			  				<h4>Contact Information</h4>
			  				<p>First Name : </p><input type="text" name="firstname" class="form-control">

			  				<p>Last Name : </p><input type="text" name="lastname" class="form-control">

			  				<p>Title :</p> <input type="text" name="title" class="form-control"
			  				placeholder="H.R Manager ceo skills requirements"> 

			  				<h4>Login Information</h4>

			  				<p>Your Business Email :</p><input type="text" name="B_Email" class="form-control">
			  				<p>Password :</p><input type="Password" name="password" class="form-control">

			  				<h4>Company Basic Information</h4>
			  				<p>Company name :</p><input type="text" name="CompName" class="form-control">
			  				<p>Company Phone :</p><input type="text" name="CompPhone" class="form-control">
			  				<p>Company website : </p> <input type="text" name="CompSite" class="form-control" 
			  				placeholder="Company URL">
			  				{{ csrf_field() }}
			  				<br/>
			  				<button type="submit" class="btn btn-info btn-block">
			  				 <span class="fa fa-send">Send</span></button>
			  			</form>
		  		</div>
	  	</div>

	  	<div class="col-md-1"></div>
  </div>
@endsection