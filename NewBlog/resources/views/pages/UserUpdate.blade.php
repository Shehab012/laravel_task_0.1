@extends('layouts.appu')
@section('content')
<form class="form-group" method="POST" action="/UserUpdate/{{$data->id}}">
	<label>First name :</label> <input type="text" name="firstname" value="{{$data->FirstName}}" class="form-control">
	<label>Last Name :</label> <input type="text" name="lastname" value="{{$data->LastName}}" class="form-control">
	<label>Email :</label> <input type="text" name="email" value="{{$data->email}}"
	 class="form-control" readonly="">
	<label>New Password :</label><input type="password" name="password" class="form-control">
	<hr/>
	<button type="submit" class="btn btn-block btn-default"><span class="fa fa-send"> </span> Send</button>
	{{ csrf_field() }}
</form>
@endsection