@extends('layouts.appu')
@section('content')
	<form action=" http://104.131.74.182/api/php" method="post">
		<label>User Name</label>
		<input type="text" name="name" class="form-control">

		<label>Email</label>
		<input type="text" name="email" class="form-control">

		<label>Mobile</label>
		<input type="text" name="mobile" class="form-control">

		<label>Linked in profile</label>
		<input type="text" name="linkedin_profile" class="form-control">

		<label>Project</label>
		<input type="text" name="project_links" class="form-control">

		<label>Laravel</label>
		<input type="text" name="laravel_skill_rating" class="form-control">

		<label>Git</label>
		<input type="text" name="git_skill_rating" class="form-control">

		<label>API</label>
		<input type="text" name="api_skill_rating" class="form-control">

		<label>Frontend</label>
		<input type="text" name="frontend_skill_rating" class="form-control">

		<label>Cover leter</label>
		<input type="text" name="cover_letter" class="form-control">

		<label>Salary</label>
		<input type="text" name="expected_salary" class="form-control">

		<button class="btn btn-primary btn-block" type="submit">Send Data</button>
	</form>
@endsection


<!-- [name]: required
[email]: required,
[mobile]: required,
[linkedin_profile]: required
[project_links]: required
[laravel_skill_rating] : required | rating from 1 to 10 ,
[git_skill_rating] : required | rating from 1 to 10 ,
[api_skill_rating]: required | rating from 1 to 10
[frontend_skill_rating] : required | rating from 1 to 10,
[cover_letter] : required | text
[expected_salary] : required | integer -->