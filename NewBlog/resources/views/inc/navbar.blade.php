<div class="container-fluied">
       <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">{{config('app.name')}}</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="home">Home</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Edit profile</a></li>
          <li><a href="#">edit CV</a></li>
          <li><a href="#">edit timeline</a></li>
        </ul>
      </li>
      <li><a href="#">Messages</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>
       </div>