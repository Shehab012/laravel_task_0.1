<div class="container-fluied">
       <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">{{config('app.name')}}</a>
    </div>
    <ul class="nav navbar-nav fixed-top">
      <li><a href="home">Home</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Profile<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="/profile/{{Auth::id()}}">My profile</a></li>
          <li><a href="#">edit my profile</a></li>
          <li><a href="#">edit timeline</a></li>
        </ul>
      </li>
      <li><a href="/posts">posts</a></li>
      <li><a href="/userstable">Users table</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> logout</a></li>
    </ul>
  </div>
</nav>
<!--
user nav bar
-->

</div>