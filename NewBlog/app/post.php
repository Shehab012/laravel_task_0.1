<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'title','body','user_id','created_at','updated_at',
    ];
    public function user_post(){
        $this->user_id = Auth::id();
    }
    public function date()
    {
        return Carbon::parse()->format('m-d-Y');
    }
}
