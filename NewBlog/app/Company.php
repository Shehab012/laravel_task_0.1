<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    //
    protected $fillable = [
    	'FirstName' , 'LastName' , 'JobTitle' ,
    	'B-Email' , 'Password' , 'CompName' ,
    	'CompPhone' , 'CompSite' , 
    	'Status' , 'Role' , 'timestamps' ,
    ];
}
