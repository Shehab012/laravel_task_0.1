<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class avatar extends Model
{
    protected $table = 'avatars';

    protected $fillable = [
    	'name','description'
    ];
}
