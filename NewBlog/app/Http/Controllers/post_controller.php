<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\post;
use App\UserApp;
use valedate;
class post_controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = post::orderBy('created_at','decs')->where('user_id',Auth::id())->get();
        return view('pages.posts')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = request();
        
        //vaidation
        $this->validate($request,[
            'title'=>'min:5|max:30',
            'body'=>'min:2|max:255',
        ]);

        //create post
        $posts = new post;

        $posts->title = $req->title;
        $posts->body = $req->body;
        $posts->user_id = Auth::id();
        
        if(!$posts){
            return back();
        }
        $posts->save();
        return redirect('posts')->with('success','your post saccssed');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post =  post::find($id);
        if ($post && $post->user_id == Auth::id()) {

            //return view('pages/post_show')->with('post',$post);
        return view('pages.post_show', ['data' => $post ] );
        }

        return redirect('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = post::find($id);
        return view('pages.post_update',[ 'data' => $post ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $posts = post::find($id);
         $req = request();

        $posts->title = $req->title;
        $posts->body = $req->body;
        $posts->user_id = Auth::id();
        
        if(!$posts){
            return back();
        }
        $posts->save();
        return redirect('posts')->with('success','your post updated saccssed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = post::find($id);
        $post->delete();

        return redirect('posts')->with('error','your post is deleted');
    }
}
