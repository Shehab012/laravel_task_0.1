<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\avatar;
use Auth;
use Image;

class AvatarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $v = avatar::all();
        return view('/profile',['image' => $v]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'Avatar' => 'image'
        ]);



        if ($request->hasfile('Avatar')) {
            $image = $request->file('Avatar');
            $filename = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('imges/'.$filename);

            $avatar = new avatar;

            if ($avatar->avatar) {
                return back()->with('error','you have already image !! try to edit your profile');
            }
            $avatar->avatar = $filename;
            $avatar->status = '1';
            $avatar->user_id = Auth::id();
            $save = $avatar->save();

            if (!$save) {
                return back()->with('error','try upload again !');
            }else{
                 image::make($image)->resize(400,500)->save($location);
                 return redirect('/profile/'.Auth::id());
            }
           
        }else{
            $filename = 'noimage.jpg';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
