<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use vaidate;

class jobcontroller extends Controller
{
    public function job(){

    	$req = request();

    	$check = $this->validate($req,[
    		'name'=>'string|required',
    		'email'=>'email|required|string',
    		'mobile'=> 'string|required',
    		'linkedin_profile'=>'required|string',
    		'project_links'=>'required|string',
    		'laravel_skill_rating'=>'integer|required|min:0|max:10',
    		'git_skill_rating'=>'integer|required|min:0|max:10',
    		'api_skill_rating'=>'integer|required|min:0|max:10',
    		'frontend_skill_rating'=>'integer|required|min:0|max:10',
    		'cover_letter'=>'string|required',
    		'expected_salary'=>'requierd|integer',
    	]);

    	if ($check) {
    		return redirect('job')->with('error');
    	}
	    	$data = new \App\Jobmodel;
		    	$data->name = $req->name;
		    	$data->email = $req->email;
		    	$data->mobile = $req->mobile;
		    	$data->linkedin_profile = $req->linkedin_profile;
		    	$data->project_links = $req->project_links;
		    	$data->laravel_skill_rating = $req->laravel_skill_rating;
		    	$data->git_skill_rating = $req->git_skill_rating;
		    	$data->api_skill_rating = $req->api_skill_rating;
		    	$data->frontend_skill_rating = $req->frontend_skill_rating;
		    	$data->cover_letter = $req->cover_letter;
		    	$data->expected_salary = $req->expected_salary;

		    	$data->save();
		    	returm back()->with('success','Send compelete');


    }
}
