<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use UserApp;
class pages extends Controller
{
    public function index(){
        $title = 'welcome to home page';
        // return view('pages.index', compact('title'));
        return view('pages.index')->with('title',$title);
    }

    public function FrontPage(){
        return view('pages.FrontPage');
    }
    public function signup(){
        return view('pages.signup');
    }
    public function login(){
        return view('pages.login');
    }
    public function UserApp(){
        $x = \App\UserApp::all()->where('App_status','complete')
        ->where('user_id',Auth::id())
        ->first();
        if($x){
            return redirect('home');
        }
        return view('pages.UApp');
    }
    public function home(){
        $x = \App\UserApp::all()->where('App_status','complete')
                                ->where('user_id',Auth::id())
                                ->first();
        if($x){
           return view('pages.home');
        }else{

            return redirect('UApp');
        }
       
    }

    public function profile(){
        return view('pages.profile');
    }

    public function job(){
        return view('pages.job');
    }

    //logout 
    public function logout(){
        Auth::logout();
        return redirect('/');
    }

}
