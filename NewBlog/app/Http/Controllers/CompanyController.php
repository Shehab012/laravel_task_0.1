<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use valedate;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.SignupAsComp');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'firstname'=>'string|min:3|max:10',
            'lastname'=>'string|min:3|max:30',
            'title'=>'string|min:3|max:10',
            'B-Email'=>'string|email|min:10|max:30',
            'password'=>'string|min:6|max:15',
            'CompName'=>'string|min:4|max:30',
            'CompPhone'=>'string|min:10|max:30',
            'CompSite'=>'string|max:300',
        ]);
        $req = request();
        $comp = new \App\company;

        $comp->FirstName = $req->firstname;
        $comp->LastName = $req->lastname;
        $comp->JobTitle = $req->title;
        $comp->BEmail = $req->B_Email;
        $comp->Password = $req->password;
        $comp->CompName = $req->CompName;
        $comp->CompPhone = $req->CompPhone;
        $comp->CompSite = $req->CompSite;
        $comp->save();
        return redirect('/ComponyDashbord')->with('success','Done');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
