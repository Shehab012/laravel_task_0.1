<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use valedate;

class UserController extends Controller
{
  public function NewUser(){
      $req = request();
      $user = new \App\User;

        $check = $this->validate($req,[
            'fname' => 'min:2|max:10|string',
            'mdname'=> 'min:2|max:15|string',
            'lname' => 'min:2|max:15|string',
            'email' => 'email|min:8|max:30',
            'password' => 'string|min:6|max:10',
        ]);

        if($check){
            return back();
        }else{
            $user->FirstName = $req->fname;
            $user->MiddleName = $req->mdname;
            $user->LastName = $req->lname;
            $user->email = $req->email;
            $user->password = bcrypt($req->password);
            $user->save();

            Auth::login($user);
            if(Auth::check()){
                return redirect('home')->with('success','Welcome' .' ' .Auth::user()->FirstName);
            }else{
                return redirect('signup')->with('error','please u have to login first');
            }
            
        
    }
  }   

  public function login(){
      $req = request();

      $check = Auth::attempt(['email'=>$req->email,'password'=>$req->password]);
      
    if(!$check){
        return back()->with('error','you must be sign-up ');
    }else{
        $user =  \App\User::where('email', $req->email)
                         ->where('password', $req->password)
                         ->first();
                         
        return redirect('home');
    }
         Auth::login($user);
  }
  public function edit($id){
    $users = \App\User::find($id);
    return view('pages.UserUpdate',['data'=>$users]);
  }

  public function update(request $request, $id){

    $users = \App\User::find($id);
    $req=request();

    //validation
    // $this->validate($req,[
    //   'firstname' => 'string|min:2|max:10',
    //   'lastname' => 'string|min:2|max:20',
    //   'email' => 'email|min:8|max:20',
    //   'password' => 'string|min:6|max:9',
    // ]);
    
      $users->FirstName = $req->firstname;
      $users->LastName = $req->lastname;
      $users->email = $req->email;
      $users->password = bcrypt($req->password);
      
       $users->save();
       return redirect('/profile/'.Auth::id())->with('success','the information is update');
    

  }

  public function userstable(){

       $users = \App\User::all();
       return view('pages.userstable',compact('users'));
  }

  public function delete($id){

    $users = \App\User::find($id);
    $users->delete();

    return redirect('/home');
  }

}
