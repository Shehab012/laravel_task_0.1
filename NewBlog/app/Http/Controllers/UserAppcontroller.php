<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use \App\UserApp;
use Auth;
use validate;

class UserAppcontroller extends Controller
{
    public function insert(){

        // request from form template
        $uapp = new \App\UserApp;
        $req = request();
        //validate
        $check = $this->validate($req,[
            'phone'=>'required|string|max:22|min:11|unique:user_apps',
            'nationality'=>'string|required',
            'city'=>'string|required',
            'gender'=>'string|required|max:6|min:4',
            'college'=>'string|required',
            'education'=>'string|required',
            'great'=>'integer|required',
            'experience'=>'string|required',
            'birthdate'=>'required|date',
            'military_status'=>'string|required',
            'Marital_Status'=>'string|required',
            'CV'=>'required',
        ]);

        if($check){
            return back()->with('errors','error');
        }else{
            // insert to userapp table
        $uapp->phone = $req->phone;
        $uapp->nationality = $req->nationality;
        $uapp->city = $req->city;
        $uapp->gender = $req->gender;
        $uapp->college = $req->college;
        $uapp->education = $req->education;
        $uapp->great = $req->great;
        $uapp->experience = $req->experience;
        $uapp->birthdate = $req->birthdate;
        $uapp->military_status = $req->military_status; 
        $uapp->Marital_Status = $req->Marital_Status;
        $uapp->CV = $req->CV;
        $uapp->user_id = $uapp->uid();
        $uapp->App_status = "complete";
        $x = $uapp->save();
        // check and save
           return redirect('/profile/'.Auth::id());
        }
        
    }
}
