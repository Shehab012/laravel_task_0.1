<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use carbon\carbon;
class UserApp extends Model
{
    protected $table = 'user_apps';
    protected $fillable = [ 
        'phone','nationality','city',
        'gender','college','education',
        'great','experience','birthdate',
        'military_status','	Marital Status','CV',
        'App_status','user_id',

    ];
    public function uid(){
        return $this->user_id = Auth::id();
    }
    // public function birthdate($BD){
    //     $BD = $this->birthdate;
    //     return Carbon::parse($BD)->format('m-d-Y');
    // }

    public function App_status(){
        App\UserApp()->App_status->get();
    }
}
