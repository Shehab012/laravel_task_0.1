<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->string('nationality');
            $table->string('city');
            $table->string('gender');
            $table->string('college');
            $table->string('education');
            $table->string('great');
            $table->string('experience')->nallable;
            $table->date('birthdate');
            $table->string('military_status');
            $table->string('Marital_Status');
            $table->text('CV');
            $table->string('App_status')->default('non');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_apps');
    }
}
