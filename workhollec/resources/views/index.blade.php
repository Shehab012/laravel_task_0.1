@include ('layouts/header');

<div class="container">
    <div class="col-md-12">
        <div class="row">
            
            <div class="col-md-6 signup">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <h2>sign up</h2>

                            <?php
                            if (session()->has('errors')) {
                                foreach (session()->get('errors')->toArray() as $k => $v) {
                                    foreach ($v as $x => $y) {
                                        echo "<div style='color:red'>{$y}</div>";
                                    }
                                }
                            }
                            ?>
                            


                        </div>
                    </div>
                    <br/>
                    <div class="col-md-12">
                        <div class="row">
                            <form class="inline-form" method="post" action="/signup">
                                <input class="form-control" type="Text" name="f_name" placeholder="Your first name " required="">
                                <input class="form-control" type="Text" name="l_name" placeholder="Your last name" required="">
                                <input class="form-control" type="email" name="email" placeholder="Your Email address" required="">
                                <input class="form-control" type="hidden" name="role" value="user" readonly="">
                                <input type="hidden" name="status" value="Active" readonly="">
                                <input class="form-control" type="password" name="password" placeholder="Password" required="">

                                <input class="form-control" type="hidden" name="_token" value= "<?php echo csrf_token(); ?>" readonly="" >
                                <button type="submit" class="btn btn-primary btn-block">Send <span class="fa fa-send"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-md-6 signup">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                        <h2>Login</h2>  
                        </div>
                    </div>
                <div class="col-md-12">
                    <div class="row">
                        <form class="inline-form" method="post" action="login">
                            <input class="form-control" type="Text" name="email" placeholder="Your Email" required="">
                            <input class="form-control" type="password" name="password" placeholder="Your Password" required="">
                            <label><input type="checkbox" name="remember_me" value="1"> Remember me</label>
                            <p><b><a href="#">forgit my password</a></b></p>
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-success btn-block">Login <span class="fa fa-user-o"></span></button>
                        </form>
                    </div>
                </div>
                </div>
            </div>




        </div>


@include('layouts/footer')