@include('/layouts/header');

<div class="container col-md-12">
	<div class="row">
		
		<div class="container usercv" style="text-align: center;">
			<h3>Your application</h3>
			<p>submit now for applay to more 10,000 jobs</p>
			<form method="post" action="/UserApp" enctype="multipart/form-data">
				<table class="table table-striped">

					<!-- 
						Table head
					-->
					<thead>
						<tr>
							<th></th>
							<th></th>
							
						</tr>
					</thead>

					<!--
						table body
					-->
					<tbody>
						<tr>
							<td><label><span class="fa fa-map-marker"></span> Address:</label></td>
							<td><input class="form-control" type="text" name="address" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-university"></span> college:</label></td>
							<td><input class="form-control" type="text" name="college" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-mortar-board"></span> certificate:</label></td>
							<td><input class="form-control" type="text" name="certificate" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-universal-access"></span> training:</label></td>
							<td><input class="form-control" type="text" name="trining" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-book"></span> courses:</label></td>
							<td><input class="form-control" type="text" name="courses" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-id-badge"></span> experince:</label></td>
							<td><input class="form-control" type="text" name="experience" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-mobile-phone"></span> phone:</label></td>
							<td><input class="form-control" type="text" name="phone" required=""></td>
						</tr>

						<tr>
							<td><label><span class="fa fa-calendar-check-o"></span>Graduation year:</label></td>
							<td><input class="form-control" type="date" date('M-D-Y') name="GR_year" required=""></td>
						</tr>


						<tr>
							<td><label><span class="fa fa-comment"></span> Comment:</label></td>
							<td><input class="form-control" type="text" name="Comment" required=""></td>
						</tr>

						<tr>
							<td class="info"><label><span class="fa fa-upload"></span> Your pic:</label></td>
							<td class="info"><input class="form-control" type="file" name="avatar" required=""></td>
						</tr>

						<tr>
							<td class="danger"><label><span class="fa fa-upload"></span> your cv:</label></td>
							<td class="danger"><input class="form-control" type="file" name="cv" required=""></td>
						</tr>


					</tbody>
					
				</table>
				{{csrf_field()}}
				<button type="submit" class="btn btn-primary btn-block"><b>Send</b><span class="fa fa-send"></span></button>
			</form>
		</div>


	</div>
</div>

@include('/layouts/footer');