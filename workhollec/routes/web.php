<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->middleware('guest');


Route::get('logout',function(){
	Auth::logout();
	return redirect('/');
});


Route::get('/home/',function(){

	return view('/home');
})->middleware('auth');





Route::post('/signup','signupController@process');

Route::post('/login','loginController@process');

Route::get('/UserApp','UserApp@view')->middleware('auth');

Route::post('/UserApp','UserApp@process');


	

Route::get('/profile', function(){
	return view('/profile');
});

Route::post('/profile', function(){
	$user =  new \App\User;
});









