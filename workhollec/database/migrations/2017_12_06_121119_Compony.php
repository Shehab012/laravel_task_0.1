<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Compony extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company',function(Blueprint $table){
            $table->increments('id');
            $table->string('com_name');
            $table->string('com_email')->unique();
            $table->string('com_address');
            $table->string('com_phone_number');
            $table->text('com_http_address');
            $table->integer('user_id')->unsigned();
            $table->rememberToken();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('company');
    }
}
