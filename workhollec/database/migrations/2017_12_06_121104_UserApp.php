<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('users_app',function(Blueprint $table){
            $table->increments('id');
            $table->string('address');
            $table->string('college');
            $table->string('certificate');
            $table->string('trining')->nullable();
            $table->string('courses')->nullable();
            $table->string('experience')->nullable();
            $table->string('phone');
            $table->integer('GR_year');
            $table->text('comment');
            $table->text('avatar')->nullable();
            $table->text('cv');
            $table->integer('user_id')->unsigned();
            $table->rememberToken();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();


        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_app');
    }
}
