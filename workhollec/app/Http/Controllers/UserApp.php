<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use Auth;
use Datetime;
class UserApp extends Controller
{
    public function view(){
    	return view('/UserApp');
    }

    public function process(){

    	$req = Request();

    	$UApp = new \App\UserApp;

    	$UApp->address = $req->address;
    	$UApp->college = $req->college;
    	$UApp->certificate = $req->certificate;
    	$UApp->trining = $req->trining;
    	$UApp->courses = $req->courses;
    	$UApp->experience = $req->experience;
    	$UApp->phone = $req->phone;
    	$UApp->GR_year = new \Datetime($req->GR_year);
    	$UApp->comment = $req->comment;
    	$UApp->avatar = \Image::make($req->avatar) ;
    	$UApp->cv = $req->cv;
        $UApp->user_id = Auth::id();

    	$save = $UApp->save();

        if ($a->fails()) {
            return 'error';
        }
        dd($a);

    }
}
