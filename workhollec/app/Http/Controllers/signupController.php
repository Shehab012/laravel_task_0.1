<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use Illuminate\Http\Request;

class signupController extends Controller
{
    public function process(){
    	$req = request();

    	$Rules = [
		'f_name'=>'string|min:5|max:30',
		'l_name'=>'string|min:5|max:40',
		'email'=>'email|min:10|max:70',
		'password'=>'string|min:5|max:20'
	];

	$v = Validator::make($req->all(),$Rules);

	if ($v->fails()) {
		session()->flash('errors', $v->errors() );

		return back();
	}else{
		$user = new \App\User;
		$user->first_name = $req->f_name;
		$user->last_name = $req->l_name;
		$user->email = $req->email;
		$user->role = $req->role;
		$user->status = $req->status;
		$user->password = bcrypt($req->password);
		$user->save();

		Auth::login($user);
		return redirect('/UserApp');
	}
    }
}
