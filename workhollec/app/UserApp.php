<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App;
use Auth;
class UserApp extends Model
{
    protected $table = 'users_app';

    protected $fileable =  [

    	'address','college','certificate',
    	'trining','courses','	experience',
    	'phone','GR_year','comment',
    	'avatar','cv','user_id',

    ];

    public function user_reli(){
    	return \App\User::find($this->user_id);
    }

}
